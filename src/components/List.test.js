import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import List from './List';

describe('List of news item::', () => {
  const items = [];
  beforeAll(() => {
    for (let i = 0; i < 10; i++) {
      items.push({ id: i, snippet: `snippet ${i}`, source: `source ${i}`, photos: [`photo ${i}`] });
    }
  });

  it('render correct structure', () => {
    expect(toJson(mount(<List items={items} />))).toMatchSnapshot();
  });

  it('click on item one to get it back', () => {
    const click = jest.fn();
    const wrapper = mount(<List items={items} onClick={click} />);
    wrapper.find('[snippet="snippet 0"]').simulate('click');
    expect(click.mock.calls.length).toBe(1);
    expect(click.mock.calls[0][0]).toBe(items[0]);
  });
});
