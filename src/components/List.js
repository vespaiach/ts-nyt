import React from 'react';
import cx from 'classnames';
import Item from './Item';

const List = ({ items, onClick, className }) => (
  <div className={cx('list', className)}>
    {items.map((it) => (
      <Item key={it.id} snippet={it.snippet} source={it.source} photo={it.photos[0]} onClick={(e) => onClick(it)} />
    ))}
  </div>
);

export default List;
