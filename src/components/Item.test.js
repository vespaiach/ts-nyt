import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Item from './Item';

describe('News item::', () => {
  it('render correct structure', () => {
    expect(toJson(mount(<Item snippet={'snippet'} source={'source'} photo={'photo'} />))).toMatchSnapshot();
  });
  it('click on item to fire onClick event', () => {
    const click = jest.fn();
    const wrapper = mount(<Item snippet={'snippet'} source={'source'} photo={'photo'} onClick={click} />);

    wrapper.simulate('click');
    expect(click.mock.calls.length).toBe(1);
  });
});
