import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Paging from './Paging';

describe('Paging::', () => {
  const pages = [];
  beforeAll(() => {
    for (let i = 0; i < 10; i++) {
      pages.push(i + 1);
    }
  });

  it('render correct structure', () => {
    expect(toJson(mount(<Paging pages={pages} current={3} />))).toMatchSnapshot();
  });

  it('click on item to get page number', () => {
    const click = jest.fn();
    const wrapper = mount(<Paging pages={pages} current={3} onClick={click} />);
    wrapper.find('li').at(1).find('button').simulate('click');
    expect(click.mock.calls.length).toBe(1);
    expect(click.mock.calls[0][0]).toEqual(1);
  });

  it('click on prev button to get prev page number', () => {
    const click = jest.fn();
    const wrapper = mount(<Paging pages={pages} current={3} onClick={click} />);
    wrapper.find('li').at(0).find('button').simulate('click');
    expect(click.mock.calls.length).toBe(1);
    expect(click.mock.calls[0][0]).toEqual(2);
  });

  it('click on next button to get next page number', () => {
    const click = jest.fn();
    const wrapper = mount(<Paging pages={pages} current={3} onClick={click} />);
    wrapper.find('li').at(11).find('button').simulate('click');
    expect(click.mock.calls.length).toBe(1);
    expect(click.mock.calls[0][0]).toEqual(4);
  });
});
