import React from 'react';

const Paging = ({ pages, total, current, onClick }) => {
  const liEls = pages.map((p) => (
    <li key={p} className={p === current ? 'active' : null}>
      <button onClick={() => onClick(p)} disabled={p === current}>
        {p}
      </button>
    </li>
  ));

  return (
    <ul className="paging">
      <li key="prev">
        <button onClick={() => onClick(current - 1)} disabled={current === 1}>
          ...
        </button>
      </li>
      {liEls}
      <li key="next">
        <button onClick={() => onClick(current + 1)} disabled={current >= total}>
          ...
        </button>
      </li>
    </ul>
  );
};

export default Paging;
