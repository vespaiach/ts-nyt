import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Dialog from './Dialog';

describe('Modal dialog::', () => {
  it('render correct structure', () => {
    expect(toJson(mount(<Dialog>test</Dialog>))).toMatchSnapshot();
  });

  it('click on close button', () => {
    const click = jest.fn();
    const wraper = mount(<Dialog onClose={click}>test</Dialog>);
    wraper.find('.close').simulate('click');
    expect(click.mock.calls.length).toBe(1);
  });
});
