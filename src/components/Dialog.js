import React from 'react';

const Dialog = ({ children, onClose }) => {
  return (
    <div className="modal">
      <div className="dimmer" />
      <div className="content">
        <button className="close" onClick={onClose} title="Close">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <g fill="none" fillRule="evenodd">
              <rect width="24" height="24" fill="#fff" fillRule="nonzero" rx="1" />
              <g stroke="#454647" strokeLinecap="square">
                <path d="M19 5L5 19M19 19L5 5" />
              </g>
            </g>
          </svg>
        </button>
        {children}
      </div>
    </div>
  );
};

export default Dialog;
