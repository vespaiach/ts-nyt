import React from 'react';

const Item = ({ snippet, source, photo, onClick }) => (
  <div className="item" onClick={onClick}>
    <article>
      <p>{snippet}</p>
      <p>{source}</p>
    </article>
    {photo && <div className="photo" style={{ backgroundImage: `url(${photo})` }} />}
  </div>
);

export default Item;
