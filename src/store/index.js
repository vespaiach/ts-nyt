import { createStore as initStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducer';

export default function createStore(initialState) {
  let enhance = (data) => data;
  if (process.env.NODE_ENV === 'development') {
    enhance = require('redux-devtools-extension/developmentOnly').composeWithDevTools;
  }

  return initStore(reducer, initialState, enhance(applyMiddleware(thunkMiddleware)));
}
