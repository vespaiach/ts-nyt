import { itemsSelector, totalSelector, pageSelector, modalDataSelector } from './selector';

describe('Selectors::', () => {
  let state;
  beforeEach(() => {
    state = {
      pages: { '1': ['_id'] },
      articles: {
        _id: {
          id: '_id',
          url: 'web_url',
          snippet: 'snippet',
          source: 'source',
          photos: ['https://www.nytimes.com/url']
        }
      },
      hits: 38918,
      current: 1,
      fetching: false,
      error: '',
      selectedId: null
    };
  });

  it('should return an empty item array', () => {
    state.current = 3;
    expect(itemsSelector(state)).toEqual([]);
  });

  it('should return item array with one element', () => {
    expect(itemsSelector(state)).toEqual([
      {
        id: '_id',
        url: 'web_url',
        snippet: 'snippet',
        source: 'source',
        photos: ['https://www.nytimes.com/url']
      }
    ]);
  });

  it('should return 3892 pages in total (with offset = 10)', () => {
    expect(totalSelector(state)).toBe(3892);
  });

  it('should return a 10 elements array from 1 to 10', () => {
    state.current = 2;
    expect(pageSelector(state)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  });

  it('should return a 10 elements array from 11 to 20', () => {
    state.current = 11;
    expect(pageSelector(state)).toEqual([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
  });

  it('should return a 10 elements array from 11 to 20', () => {
    state.selectedId = '_id';
    expect(modalDataSelector(state)).toEqual({
      id: '_id',
      url: 'web_url',
      snippet: 'snippet',
      source: 'source',
      photos: ['https://www.nytimes.com/url']
    });
  });
});
