import reducer from './reducer';
import { TYPES } from './action';

describe('Reducers::', () => {
  const dumpResponseData = {
    docs: [{ web_url: 'web_url', snippet: 'snippet', source: 'source', multimedia: [{ url: 'url' }], _id: '_id' }],
    meta: { hits: 38918, offset: 0, time: 20 }
  };

  it('should return expected data after loading successfully', () => {
    const expected = {
      pages: { '1': ['_id'] },
      articles: {
        _id: {
          id: '_id',
          url: 'web_url',
          snippet: 'snippet',
          source: 'source',
          photos: ['https://www.nytimes.com/url']
        }
      },
      hits: 38918,
      current: 1,
      fetching: false,
      error: '',
      selectedId: null
    };
    const action = {
      type: TYPES.FETCH_DATA_SUCCESS,
      data: dumpResponseData
    };

    expect(reducer(undefined, action)).toEqual(expected);
  });

  it('should set current page to 2', () => {
    const action = {
      type: TYPES.GO_TO_PAGE,
      page: 2
    };

    expect(reducer(undefined, action).current).toBe(2);
  });

  it('should set fetching to true', () => {
    const action = { type: TYPES.FETCHING };

    expect(reducer(undefined, action).fetching).toBe(true);
  });

  it('should set error and fetching to false', () => {
    const action = { type: TYPES.FETCH_DATA_FAIL };
    const state = reducer(undefined, action);

    expect(state.fetching).toBe(false);
    expect(state.error).toBe('API error');
  });

  it('should set selected id to null', () => {
    const action = { type: TYPES.CLOSE_DIALOG };
    const state = reducer(undefined, action);

    expect(state.selectedId).toBe(null);
  });

  it('should set selected id to 1', () => {
    const action = { type: TYPES.OPEN_DIALOG, articleId: 1 };
    const state = reducer(undefined, action);

    expect(state.selectedId).toBe(1);
  });
});
