import config from '../config';

const TYPES = {
  FETCH_DATA_SUCCESS: 'FETCH_DATA_SUCCESS',
  GO_TO_PAGE: 'GO_TO_PAGE',
  FETCHING: 'FETCHING',
  FETCH_DATA_FAIL: 'FETCH_DATA_FAIL',
  OPEN_DIALOG: 'OPEN_DIALOG',
  CLOSE_DIALOG: 'CLOSE_DIALOG'
};
export { TYPES };

export function fetchPage(page) {
  return async dispatch => {
    try {
      dispatch({ type: TYPES.FETCHING });
      const response = await fetch(`${config.URL}${config.API}?api-key=${config.TOKEN}&q=singapore&page=${page}`);
      const data = await response.json();
      dispatch({ type: TYPES.FETCH_DATA_SUCCESS, data: data.response });
    } catch (error) {
      dispatch({ type: TYPES.FETCH_DATA_FAIL, error });
    }
  };
}

export function gotoPage(page) {
  return async (dispatch, getState) => {
    const state = getState();
    if (state.pages && state.pages[page]) {
      dispatch({ type: TYPES.GO_TO_PAGE, page });
    } else {
      dispatch(await fetchPage(page - 1));
    }
  };
}

export function closeDialog() {
  return {
    type: TYPES.CLOSE_DIALOG
  };
}

export function openDialog(articleId) {
  return {
    type: TYPES.OPEN_DIALOG,
    articleId
  };
}
