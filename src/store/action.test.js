import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import config from '../config';
import { TYPES, fetchPage, gotoPage, closeDialog, openDialog } from './action';

const mockStore = configureMockStore([thunk]);

describe('Action creators::', () => {
  const dumpResponseData = {
    docs: [{ web_url: 'web_url', snippet: 'snippet', source: 'source', multimedia: [], _id: '_id' }],
    meta: { hits: 38918, offset: 0, time: 20 }
  };

  fetchMock.get(`${config.URL}${config.API}?api-key=${config.TOKEN}&q=singapore&page=0`, {
    body: { response: dumpResponseData },
    headers: { 'content-type': 'application/json' }
  });

  it('should create an action to close dialog', () => {
    expect(closeDialog()).toEqual({ type: TYPES.CLOSE_DIALOG });
  });

  it('should create an action to open dialog', () => {
    expect(openDialog(1)).toEqual({ type: TYPES.OPEN_DIALOG, articleId: 1 });
  });

  it('should send action FETCHING then call api to load page data and return action FETCH_DATA_SUCCESS ', () => {
    const expectedActions = [
      { type: TYPES.FETCHING },
      {
        type: TYPES.FETCH_DATA_SUCCESS,
        data: dumpResponseData
      }
    ];
    const store = mockStore({});

    return store.dispatch(fetchPage(0)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should send action GO_TO_PAGE', () => {
    const store = mockStore({ pages: { 0: [1] } });
    const expectedActions = [{ type: TYPES.GO_TO_PAGE, page: 0 }];
    store.dispatch(gotoPage(0));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should send action GO_TO_PAGE', () => {
    const expectedActions = [{ type: TYPES.FETCHING }];
    const store = mockStore({});

    return store.dispatch(gotoPage(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
