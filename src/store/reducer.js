import update from 'immutability-helper';

import { TYPES } from './action';
import config from '../config';

const defaultState = {
  pages: {},
  articles: {},
  hits: 0,
  current: 0,
  fetching: false,
  error: '',
  selectedId: null
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case TYPES.FETCH_DATA_SUCCESS: {
      const itemIds = [];
      const articles = action.data.docs.reduce((acc, item) => {
        itemIds.push(item._id);
        acc[item._id] = {
          id: item._id,
          url: item.web_url,
          snippet: item.snippet,
          source: item.source,
          photos: item.multimedia.map(p => `https://www.nytimes.com/${p.url}`)
        };
        return acc;
      }, {});
      const page = Math.floor(action.data.meta.offset / config.OFFSET) + 1;
      return update(state, {
        articles: { $merge: articles },
        pages: { $merge: { [page]: itemIds } },
        current: { $set: page },
        fetching: { $set: false },
        hits: { $set: action.data.meta.hits }
      });
    }
    case TYPES.GO_TO_PAGE:
      return update(state, { current: { $set: action.page } });
    case TYPES.FETCHING:
      return update(state, { fetching: { $set: true } });
    case TYPES.FETCH_DATA_FAIL:
      return update(state, { fetching: { $set: false }, error: { $set: 'API error' } });
    case TYPES.CLOSE_DIALOG:
      return update(state, { selectedId: { $set: null } });
    case TYPES.OPEN_DIALOG:
      return update(state, { selectedId: { $set: action.articleId } });
    default:
      return state;
  }
}
