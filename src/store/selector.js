import { createSelector } from 'reselect';
import config from '../config';

const itemsSelector = createSelector(
  [state => state.current, state => state.pages, state => state.articles],
  (current, pages, articles) => {
    if (pages[current] && pages[current].length) {
      return pages[current].map(id => articles[id]);
    }
    return [];
  }
);

const totalSelector = createSelector(
  state => state.hits,
  hits => (hits % config.OFFSET === 0 ? Math.floor(hits / config.OFFSET) : Math.floor(hits / config.OFFSET) + 1)
);

const pageSelector = createSelector([totalSelector, state => state.current], (hits, current) => {
  const pages = [];
  const total = hits % config.OFFSET === 0 ? Math.floor(hits / config.OFFSET) : Math.floor(hits / config.OFFSET) + 1;
  let startSeg = current - 1 - ((current - 1) % config.SEGMENT_LENGTH);
  startSeg = startSeg < 0 ? 0 : startSeg;
  let endSeg = Math.min(startSeg + config.SEGMENT_LENGTH, total);
  for (let i = startSeg; i < endSeg; i++) {
    pages.push(i + 1);
  }
  return pages;
});

const modalDataSelector = createSelector(
  [state => state.articles, state => state.selectedId],
  (articles, id) => articles[id]
);

export { itemsSelector, pageSelector, totalSelector, modalDataSelector };
