import React from 'react';
import { connect } from 'react-redux';
import { closeDialog } from '../store/action';
import { modalDataSelector } from '../store/selector';
import Dialog from '../components/Dialog';
import Item from '../components/Item';

class ModalDialog extends React.Component {
  handleClose = () => {
    this.props.closeDialog();
  };

  render() {
    const { data } = this.props;
    if (!data) {
      return null;
    }
    return (
      <Dialog onClose={this.handleClose}>
        <Item snippet={data.snippet} source={data.source} photo={data.photos[0]} />
      </Dialog>
    );
  }
}

export default connect(
  state => ({
    data: modalDataSelector(state)
  }),
  { closeDialog }
)(ModalDialog);
