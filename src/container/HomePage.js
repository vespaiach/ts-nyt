import React from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { fetchPage, gotoPage, openDialog } from '../store/action';
import { pageSelector, itemsSelector, totalSelector } from '../store/selector';
import List from '../components/List';
import Paging from '../components/Paging';

class HomePage extends React.Component {
  componentDidMount() {
    this.props.fetchPage(0);
  }

  gotoPage = (page) => {
    if (page < 0 || page > this.props.total || page === this.props.current) {
      return;
    }
    this.props.gotoPage(page);
  };

  handleItemClick = (item) => {
    this.props.openDialog(item.id);
  };

  render() {
    const { items, pages, current, total, fetching } = this.props;

    return (
      <div className={cx('container', { fetching })} name="#top">
        <h2>Trusting Social | Pre-screening Exercise</h2>
        <p className="spinner">Loading...</p>
        <List items={items} onClick={this.handleItemClick} className={cx({ hide: fetching })} />
        <div className={cx('item', { hide: fetching })}>
          <Paging pages={pages} current={current} onClick={this.gotoPage} total={total} />
          <a href="#top" className="top">
            top
          </a>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    items: itemsSelector(state),
    pages: pageSelector(state),
    current: state.current,
    total: totalSelector(state),
    fetching: state.fetching
  }),
  { fetchPage, gotoPage, openDialog }
)(HomePage);
