import React from 'react';
import fetchMock from 'fetch-mock';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import createStore from '../store';
import config from '../config';
import page1 from '../__sampleData/dump1.json';
import page2 from '../__sampleData/dump2.json';
import HomePage from './HomePage';

describe('Action creators::', () => {
  jest.useFakeTimers();
  fetchMock.get(`${config.URL}${config.API}?api-key=${config.TOKEN}&q=singapore&page=0`, {
    body: page1,
    headers: { 'content-type': 'application/json' }
  });
  fetchMock.get(`${config.URL}${config.API}?api-key=${config.TOKEN}&q=singapore&page=1`, {
    body: page2,
    headers: { 'content-type': 'application/json' }
  });
  const store = createStore();
  const wrapper = mount(
    <Provider store={store}>
      <React.Fragment>
        <HomePage />
      </React.Fragment>
    </Provider>
  );

  it('should render without error and corect structure', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should load data correctly after component mounted', () => {
    expect(store.getState().hits).toBe(page1.response.meta.hits);
    expect(store.getState().current).toBe(1);
    expect(store.getState().pages['1']).toEqual(page1.response.docs.map((d) => d._id));
  });

  it('should set current page to 1', () => {
    wrapper.mount();
    expect(wrapper.find('.paging li.active').text()).toBe('1');
  });

  it('should load second page correctly', () => {
    wrapper
      .find('.paging li')
      .at(2)
      .find('button')
      .simulate('click');
    wrapper.mount();
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
