export default {
  URL: 'https://api.nytimes.com',
  API: '/svc/search/v2/articlesearch.json',
  TOKEN: '5763846de30d489aa867f0711e2b031c',
  SEGMENT_LENGTH: 10,
  OFFSET: 10
};
