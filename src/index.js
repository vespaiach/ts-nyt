import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createStore from './store';

import './styles/normalize.css';
import './styles/fonts.css';
import './styles/main.css';

import HomePage from './container/HomePage';
import ModalDialog from './container/ModalDialog';

ReactDOM.render(
  <Provider store={createStore()}>
    <React.Fragment>
      <HomePage />
      <ModalDialog />
    </React.Fragment>
  </Provider>,
  document.getElementById('root')
);
